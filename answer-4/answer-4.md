### Question 4.1

```
#define MIN(a,b) (a < b ? a : b)
```

it defines a function like macro, which can be used to find the minimum number between two numbers.
the parameter list accepts any type we pass, it is upto the implementation to define a proper handling otherwise we may get
not matching operands or totally false output for the type we are handling.

we should also protect the multi declarations, as below

```
#ifndef MIN
#define MIN(a,b) (a < b ? a : b)
#endif
```

moreover the modern way of handling this would be using template function, as below.

```
template <typename T>
T min(T a, T b)
{
    return a < b ? a : b;
}
```
---

### Question 4.2

```
char const* const SomeString = "A C coding test during an interview at Elgato";
size_t Length = strlen(SomeString);
char* Dest = (char*)malloc(Length);
strcpy(Dest, SomeString);
```

We create a character string, calculate the length, do the dynamic allocation of the size that we calculated in previous step.
The raw pointer returned by maclloc should be casted to pointer to char inorder to save it inside a variable of type pointer to char (Dest).
Calling `strcpy` function which will copy the source string (SomeString) to destination string (Dest).
type of `SomeString` should be const char* const.

---

### Question 4.3

```
const char* API_FormatDateTimeString(const char* inDate)
{
    struct tm date;
    char formatted_date[60];
    strptime(inDate, "%m/%d/%Y", &date);
    strftime(formatted_date, 68, "%B %d, %Y", &date);
    debug_console_puts(formatted_date);
    return (const char*)&formatted_date;
}
```

we can't return a reference of a variable that is scoped to the function and gets destroyed when we go out of the function
we have to allocate a dynamic memory through malloc and return the resource pointer, so we can use the return value of the
API outside the API function.

And it sould also check for invalid date value.

```
#include <time.h>
#include <stdio.h>
#include <memory.h>

char* API_FormatDateTimeString(const char* inDate)
{
    struct tm date;
    char* formatted_date = (char*)malloc(sizeof(date));
    strptime(inDate, "%m/%d/%Y", &date);
    strftime(formatted_date, 68, "%B %d, %Y", &date);
    puts(formatted_date);
    return formatted_date;
}

int main()
{
    char* date = "02/29/2020";
    char* result = API_FormatDateTimeString(date);
    puts(result);

    return 0;
}
```
