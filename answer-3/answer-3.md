### Question 3.1

```
var i = 5;
for(var i = 0; i < 10; i++) {
 // Do something...
}
console.log(i);
```

The output at the console log is 10.
Because the var has global scope and it is being redefined in the for loop and when we come out of for loop the value will be whatever we have at the end of the loop, i,e 10.

TO keep the global scoped variable seperate from the loop variables, we can define the loop variables with `let`, which will have local scope.

```
var i = 5;
for(let i = 0; i < 10; i++) {
 // Do something...
}
console.log(i);
```
---

### Question 3.2

```
Game.prototype.restart = function () {
 this.timer = setTimeout(function() {
 this.clearBoard();
 }, 1000);
};
```

Calling restart will call the `clearBoard` function after the timeout of 1 second.
setTimeout will return the timeout object and it will be stored in timer variable, which is helpful if we want to clear the timeout.
As an improvement, we can pass the timeout from outside and override the deafult vlaue set for the setTimeout function, which
will enable us to set the intended values and not just rely on the default value of 1 second.
also we can check if there are any previous timeout objects and clear them before making a new timeout call.

---


### Question 3.3

```
let state;
if (state == null) {
    // Do something...
}
```

state variable has been declared but not assigned any value so it will be undefined. So `if statement` evaluation will be false
so the body of if statement will not be processed. we can fix this in two ways
1. assign `null` to state when it is declared. or
2. in the if condition check against `undefined`  
