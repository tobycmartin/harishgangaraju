```
int myFunction(const std::vector<int>& vector1, const std::vector<int>& vector2)
{
    auto result = 0;

    for (const auto& i : vector1)
    {
        bool greaterValueFound = any_of(vector2.begin(), vector2.end(), [&i](const int val) {
            return i > val;
            });

        if (!greaterValueFound)
          result++;
    }

    return result;
}
```
