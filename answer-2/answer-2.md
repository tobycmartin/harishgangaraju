It is not so obvious what is wrong here, without the extra information.
But one condition where it can break is out of bound access through the arrays subscript operator.

so I would throw exception from the subscript operator if that happens. In the catch block I will just print the error and clear the matrix
and exit. I have implemented the demonstration code as below.


```
#include <array>
#include <iostream>

template <typename T, size_t NR, size_t NC>
class matrix
{
private:
    using row_container_t = std::array<T, NC>;
    using container_t = std::array<row_container_t, NR>;
    class row_proxy
    {
    public:
        row_proxy(row_container_t& irow_container) : row_container(irow_container) {}
        T& operator[](size_t col) const {
            if (col > NC)
                std::cerr << "column index out of bound" << " given: " << col << " col size: " << NC << std::endl;
                // better solution is to throw exception and handle where the matrix object is created

                return row_container[col];
        }

    private:
        row_container_t& row_container;
    };

public:
    row_proxy operator[](size_t row) {
        if (row > NR)
            std::cerr << "row index out of bound" << " given: " << row << " row size: " << NR << std::endl;
                // better solution is to throw exception and handle where the matrix object is created

        return row_proxy(container[row]);
    }

    void clear()
    {
        container_t dummy;
        container.swap(dummy);
    }

private:
    container_t container;
};

int main()
{
    const size_t rows = 3;
    const size_t cols = 3;
    matrix<std::string, rows, cols> m;
    try {
        for (size_t row = 0; row < rows; row++)
            for (size_t col = 0; col < cols; col++)
            {
                m[row][col] = std::to_string(row) + "," + std::to_string(col);
                std::cout << m[row][col] << std::endl;
            }
    }
    catch (...)
    {
        // here I am catching all exception just for demonstration
        // in reality we should catch it by thrown exception type and handle according to the type
    }

    return 0;
}
```