### Question 5

#### what code does
Basically if you give a string, the function will make the last character of the string upper case and prints it.
To achieve that it goes through each character in the string and makes the characters uppercase, after making the character uppercase, since it has to assign the value to `lastCharacter` which is `const char*`, it converts value to bytearray and then calls `constData` function on that, which converts the value to `const char*` type. the result is then stored inside `lastCharacter` and same will be printed to the console.

#### problem
1. we don't need the loop.
2. The bytearray points to the local variable `localVariable`. So when we go out of for loop it is not guranteed that the value will still point to that `localVariable`.
3. the flow is not correct, the problem in hand can be solved more easily.

#### improvements
1. there is no `argc` and `argv` that is visible to the function body, we can get rid of it Q_UNUSED for those  

The entire flow can be changed, we can get rid off the for loop. The whole process can be done in three steps.  
2. use `back` to get the last character of the string.  
3. use `toUpper` to make the character uppercase.    
4. print the value we got from step 2.  

```
#include <QCoreApplication>
#include <QString>
#include <QDebug>
#include <QStringLiteral>

void printLastCharacterUppercase( )
{
 QString hello = QStringLiteral("Helloasdfaadsfsdfs");

 qDebug() << "original string:" << hello << endl;

 auto lastCharacter = hello.back();
 qDebug() << "last character:" << lastCharacter << endl;

 auto upperLastCharacter = lastCharacter.toUpper();
 qDebug() << "uppercase last character:" << upperLastCharacter;

 Q_ASSERT(hello == "Helloasdfaadsfsdfs");
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    printLastCharacterUppercase();

    return a.exec();
}

```