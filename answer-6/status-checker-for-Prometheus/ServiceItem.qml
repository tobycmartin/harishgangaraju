import QtQuick 2.0

Rectangle {
    id: root

    property alias text: actualText.text
    property alias heading: headingText.text

    height: parent.height
    color: "#2D5B65"
    radius: 5
    border.color: "black"
    border.width: 2

    Column {
        anchors.fill: parent
        anchors.margins: 10
        spacing: 5

        Text {
            id: headingText

            anchors.horizontalCenter: parent.horizontalCenter
            color: "black"
            wrapMode: Text.WordWrap
            font.pixelSize: 15
            font.weight: Font.Normal
        }

        Text {
            id: actualText

            anchors.horizontalCenter: parent.horizontalCenter
            color: "black"
            wrapMode: Text.WordWrap
            font.pixelSize: 15
            font.weight: Font.Normal
        }
    }
}
