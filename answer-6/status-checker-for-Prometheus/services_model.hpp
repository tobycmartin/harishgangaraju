#pragma once

#include "services.hpp"

#include <QAbstractListModel>
#include <QTimer>
#include <QSet>

class NetworkManager;

class ServicesModel : public QAbstractListModel
{
    Q_OBJECT

public:
    ServicesModel(QObject* parent = nullptr);

    enum ServicesRoles
    {
        TargetRole = Qt::UserRole + 1,
        InstanceRole,
        HealthRole,
    };

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private slots:
    void handleResults(const QString& target, const QString& instance, const QString& health);

signals:
    void newRequestRunning();
    void requestProcessed();

private:
    QVector<Services*> _services;
    QSet<QString> _availableTargets;
    NetworkManager* _manager;
    QTimer _timer;
};
