import QtQuick 2.12
import QtQuick.Window 2.12
import Qt.labs.settings 1.0

Window {
    id: root
    visible: true
    minimumWidth: 920
    minimumHeight: 640
    title: qsTr("status-checker-for-Prometheus-services")
    color: "#292929"

    Homescreen {
        id: homeScreen
    }
}
