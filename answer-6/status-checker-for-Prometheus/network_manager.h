#define pragma once

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

class NetworkManager : public QObject
{
    Q_OBJECT

public:
    NetworkManager(QObject* parent = nullptr);

public slots:
    void requestData() const;

private slots:
    void processReply(QNetworkReply* reply);

signals:
    void resultReady(const QString& target, const QString& instance, const QString& health);

private:
    QNetworkAccessManager* _manager;
};
