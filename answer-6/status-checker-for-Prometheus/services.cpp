#include "services.hpp"

#include <QDebug>

Services::Services(const QString& target, const QString& instance, const QString& health, QObject* parent)
    : QObject{parent}, _target{target}, _instance{instance}, _health{health}
{
    if (target.isEmpty() || instance.isEmpty() || health.isEmpty())
        qWarning() << "services data is incomplete!" << target << health << instance << endl;
}

void Services::setInstance(const QString& instance)
{
    if (instance == _instance)
        return;

    _instance = instance;
}

void Services::setHealth(const QString& health)
{
    if (health == _health)
        return;

    _health = health;
}
