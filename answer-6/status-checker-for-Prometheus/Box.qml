import QtQuick 2.12
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.12

Item {
    id: root

    implicitHeight: 100
    implicitWidth: parent.width
    anchors.horizontalCenter: parent.horizontalCenter

    property string pTarget
    property string pInstance
    property string pHealth

    Rectangle {
        id: mainRect

        anchors.fill: parent
        color: "transparent"
        anchors.margins: 15
        radius: 5

        Row {
            height: parent.height
            spacing: 3

            ServiceItem {heading: "Taarget"; text: root.pTarget; width: mainRect.width/3 - parent.spacing}
            ServiceItem {heading: "Instance"; text: root.pInstance; width: mainRect.width/3 - parent.spacing}
            ServiceItem {heading: "Health"; text: root.pHealth; width: mainRect.width/3 - parent.spacing}
        }
    }
}
