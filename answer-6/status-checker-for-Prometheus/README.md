This is an application which queries the health status of targets of the end point "http://demo.robustperception.io:9090".

NOTE: Due to time constraint of 1 hour(eventough I took little more than 1 hour) it is being kept very minimalistic. We can make improvements for GUI as well as  functional parts. For more details see the section `Limitations`.  

Tested with Qt 5.12.7 on MacOS Catalina

## Building
NOTE: you should point to the Qt5 include directories, which you can do by exporting the `Qt5_DIR` variable.
Ex: Qt5_DIR="/Users/bob/Qt/5.12.7/clang_64/lib/cmake/Qt5"

1. makdir build
2. cd build
3. cmake ../.
4. make
5. execute generated application binary

## Limitations   
1. The udate of existing services status needs to be tested
2. The exception handling should be inroduced for network manager object
3. The network request handling should be done on a seperate thread
4. The GUI is very basic and it needs to get some beautification
