#include "network_manager.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

namespace {
const auto API = QString{"http://demo.robustperception.io:9090/api/v1/targets"};
}

NetworkManager::NetworkManager(QObject* parent) : QObject{parent}
{
    _manager = new QNetworkAccessManager{this};
    connect(_manager, &QNetworkAccessManager::finished, this, &NetworkManager::processReply);
}

void NetworkManager::requestData() const
{
    qDebug() << "requesting new set of data" << endl;

    if (!_manager)
    {
        qCritical() << "manager is absent, can't make a request" << endl;
        return;
    }

    _manager->get(QNetworkRequest(QUrl{API}));
}

void NetworkManager::processReply(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError)
        qWarning() << "error occured while processing the network reply. Error: " << reply->error();

    QJsonParseError jsonError;
    auto document = QJsonDocument::fromJson(reply->readAll(), &jsonError);
    if (jsonError.error != QJsonParseError::NoError){
        qWarning() << jsonError.errorString();
    }

    QJsonObject rootObj = document.object();

    if (rootObj.isEmpty())
    {
        qWarning() << "JSON data received is empty, aborting" << endl;
        return;
        // need to throw an exception
    }

    auto value = rootObj.value("data").toObject().value("activeTargets");

    for (const auto& i : value.toArray())
    {
        auto labels = i.toObject().value("labels");
        auto health = i.toObject().value("health");
        auto instance = labels.toObject().value("instance");
        auto target = labels.toObject().value("job");

        emit resultReady(target.toString(), instance.toString(), health.toString());
    }
}
