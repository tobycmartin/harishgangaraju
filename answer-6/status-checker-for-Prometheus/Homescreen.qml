import QtQuick 2.12
import ServicesModel 0.1
import QtQuick.Controls 2.12

Item {
    id: root

    anchors.fill: parent
    
    Rectangle {
        id: listViewItem
        
        anchors.fill: parent
        anchors.margins: 20
        color: "#404040"
        border.width: 1
        border.color: "#504848"
        radius: 5

        Text {
            id: headingText

            anchors.horizontalCenter: parent.horizontalCenter
            text: "Prometheus services watchdog"
            color: "black"
            wrapMode: Text.WordWrap
            font.pixelSize: 40
            font.weight: Font.Normal
        }
        
        ListView {
            id: listView
            
            anchors.top: headingText.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: 40
            spacing: 10
            
            model: ServicesModel {
                id: servicesModel

                onNewRequestRunning: busyIndicator.running = true
                //                onRequestProcessed: busyIndicator.running = false
            }

            delegate:  Box {
                pTarget: target
                pInstance: instance
                pHealth: health
            }

            add: Transition {
                NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 300 }
                NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 300 }
            }

            ScrollBar.vertical: ScrollBar {id: scrollBar }
        }

        Rectangle {
            id: indicatorRect

            anchors.left: listViewItem.left
            anchors.top: listViewItem.top
            width: 200
            height: 200
            anchors.leftMargin: 10
            anchors.topMargin: 10
            color: "transparent"

            BusyIndicator {
                id: busyIndicator

                anchors.horizontalCenter: parent.horizontalCenter
            }

            Text {
                id: messageText

                anchors.top: busyIndicator.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                text: "auto refresh every 30 seconds"
                wrapMode: Text.WordWrap
                font.pixelSize: 15
                font.weight: Font.Normal
            }
        }

        Text {
            id: servicesCountText

            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            text: "There are " + listView.count + " serevices"
            wrapMode: Text.WordWrap
            font.pixelSize: 20
            font.weight: Font.Normal
        }
    }
}
