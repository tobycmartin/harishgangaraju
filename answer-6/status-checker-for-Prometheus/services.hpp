#pragma once

#include <QObject>
#include <QString>

class Services : public QObject
{
    Q_OBJECT

public:
    Services(const QString& target, const QString& instance, const QString& health, QObject* parent = nullptr);

    QString target() const {return _target;}
    QString instance() const {return _instance;}
    void setInstance(const QString& instance);
    QString health() const {return  _health;}
    void setHealth(const QString& health);

private:
    QString _target;
    QString _instance;
    QString _health;
};
