#include "services_model.hpp"
#include "services.hpp"
#include "network_manager.h"

#include <QDebug>
#include <chrono>

namespace  {
using namespace std::chrono_literals;
}

ServicesModel::ServicesModel(QObject* parent) : QAbstractListModel(parent), _timer{this}
{
    _manager = new NetworkManager{this};
    connect(_manager, &NetworkManager::resultReady, this, &ServicesModel::handleResults);

    // fetch once first before doing the same in regular intervals
    _manager->requestData();

    // now call request data in regular intervals of 30 seconds.
    connect(&_timer, &QTimer::timeout, _manager, &NetworkManager::requestData);
     _timer.start();
    _timer.setInterval(30s);

    qDebug() << "services model is created" << endl;
}

int ServicesModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return _services.size();
}

QVariant ServicesModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return {};

    const auto& box      = _services.at(index.row());
    switch (role)
    {
    case TargetRole:
        return box->target();
    case InstanceRole:
        return box->instance();
    case HealthRole:
        return box->health();
    }

    return {};
}

// the results came from the managers reply
void ServicesModel::handleResults(const QString& target, const QString& instance, const QString& health)
{
    if (_availableTargets.size() == 0 || std::find(_availableTargets.begin(), _availableTargets.end(), target) == _availableTargets.end())
    {
        _availableTargets << target;

        const auto insertPos = rowCount();
        beginInsertRows(QModelIndex(), insertPos, insertPos);

        auto selected = false;
        if (_services.size() == 0)
            selected = true;

        const auto box = new Services{target, instance, health, this};
        _services.push_back(box);
        endInsertRows();
    }
    else
    {
        for (auto i : _services)
        {
            if (i->target() == target && (i->health() != health || i->instance() != instance))
            {
                i->setInstance(instance);
                i->setHealth(health);
                emit dataChanged(QModelIndex(), QModelIndex());
            }
        }
    }

    // currently this s not used in qml, the idea was to stop the busy indicator with this. but to show that there are
    // repeated requests, the busy indicator has been left running continuously
    emit requestProcessed();
}

QHash<int, QByteArray> ServicesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TargetRole] = "target";
    roles[InstanceRole] = "instance";
    roles[HealthRole] = "health";
    return roles;
}
