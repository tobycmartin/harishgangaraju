# Structure of the project

The project has been arranged as different answers in their own dedicated directory.

Elgato
- README.md
- answer-1
	- answer-1.md
- answer-2
	- answer-2.md
- answer-3
	- answer-3.md
- answer-4
	- answer-4.md
- answer-5
	- answer-5.md
- answer-6
	- status-checker-for-Prometheus
		- CMakeLists.txt
		- README.md
		- Box.qml
		- ServiceItem.qml
		- HomeScreen.qml
		- network_manager.h
		- network_manager.cpp
		- services_model.h
		- services_model.cpp
		- services.hpp
		- services.cpp
		- main.cpp


So as you can see each of the answer directory contains a `md` file consisting of answer to particular question. Except for the `answer-6`.

`answer-6` consists of a `CMAKE` project which is built using `Qt`. Since I am not a webdev, I thought I can implement the task with what I know with `C++ and Qt`. So I am using the Qt's high level network module to make the network request and get the JSON object and then I parse the JSON object to get the relevent target details. The entire thing is implemented using `model-view-controller pattern`. Since the project asked to follow a time limit of 1 hour, I could not implement the project to how I would have wanted it to be. It needs extra bit of care to make it really useful.
